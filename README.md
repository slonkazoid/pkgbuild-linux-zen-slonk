# linux-zen-slonk

slightly modified linux-zen PKGBUILD inspired by linux-cachyos

soft fork of [linux-zen](https://gitlab.archlinux.org/archlinux/packaging/packages/linux-zen)

# differences

- has options for llvm, ccache, localmodconfig, and n/menu/x/gconfig
- doesn't build docs
