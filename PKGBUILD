# Maintainer: slonkazoid <slonkazoid@slonk.ing>

# ==== OPTIONS ====
# these options are applied from top to bottom

_ccache=${_ccache-}
_llvm=${_llvm-}

# https://wiki.archlinux.org/title/Modprobed-db
_localmodconfig=${_localmodconfig-}
# set this to the path of modprobed.db, defaults to $XDG_CONFIG_HOME/modprobed.db
_lsmod=${_lsmod-${XDG_CONFIG_HOME:-$HOME/.config}/modprobed.db}

_nconfig=${_nconfig-}
_menuconfig=${_menuconfig-}
_xconfig=${_xconfig-}
_gconfig=${_gconfig-}

# ==== END OPTIONS ====

# linux-zen Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

# ==== OPTIONS ====
# these options are applied from top to bottom

_ccache=${_ccache-}
_llvm=${_llvm-}

# https://wiki.archlinux.org/title/Modprobed-db
_localmodconfig=${_localmodconfig-}
# set this to the path of modprobed.db, defaults to $XDG_CONFIG_HOME/modprobed.db
_lsmod=${_lsmod-${XDG_CONFIG_HOME:-$HOME/.config}/modprobed.db}

_nconfig=${_nconfig-}
_menuconfig=${_menuconfig-}
_xconfig=${_xconfig-}
_gconfig=${_gconfig-}

# ==== END OPTIONS ====

pkgbase=linux-zen-slonk
pkgver=6.8.2.zen2
pkgrel=1
pkgdesc='Linux ZEN slonked'
url='https://github.com/zen-kernel/zen-kernel'
arch=(x86_64)
license=(GPL-2.0-only)
makedepends=(
  bc
  cpio
  gettext
  libelf
  pahole
  perl
  python
  tar
  xz
)
options=(
  !debug
  !strip
)
_srcname=linux-${pkgver%.*}
_srctag=v${pkgver%.*}-${pkgver##*.}
source=(
  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
  $url/releases/download/$_srctag/linux-$_srctag.patch.zst{,.sig}
  config  # the main kernel config file
)
validpgpkeys=(
  ABAF11C65A2970B130ABE3C479BE3E4300411886  # Linus Torvalds
  647F28654894E3BD457199BE38DBBDC86092693E  # Greg Kroah-Hartman
  83BC8889351B5DEBBB68416EB8AC08600F108CDF  # Jan Alexander Steffens (heftig)
)
# https://www.kernel.org/pub/linux/kernel/v6.x/sha256sums.asc
sha256sums=('9ac322d85bcf98a04667d929f5c2666b15bd58c6c2d68dd512c72acbced07d04'
            'SKIP'
            '86008bcb16cead3a16baca444a60cdeda3a92ab1e8b8f403328a427c033601d7'
            'SKIP'
            '256aff039086611bcf936b617e7e3f3d2947e6cc88bcbd5bd3e790b28cfc608b')
b2sums=('f057c2512040600fbf0df67cf9c7200aee0c06b82b3cf749be8c5685844d2662a585ce17685c7af880c0d9dbbbd81302e5a1fa41c3dbd39869123121a0e82dc2'
        'SKIP'
        '850efa8e124509c7560fbb2f84ca1c49c2d0b7d6d2e0ecf764fde66a141f52ff0e94706711c47e4e6c7c48640c12accc9ae04d90a349c163f629d168f3e06f9d'
        'SKIP'
        'a95fb00f212ab2ab232b804b9300a96adfacfc6c41fd5e4c3ff35e8047640ddefeb52c4e8f41ebb66fe9b11cdc8fae222d2f9c46cc87f18a21be35600b55667b')

export KBUILD_BUILD_HOST=archlinux
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

BUILD_FLAGS=()

if [[ ${_ccache:+x} == x ]]; then
  makedepends+=(ccache)
fi

if [[ ${_llvm:+x} == x ]]; then
  makedepends+=(clang llvm lld)
  BUILD_FLAGS+=(
    CC=${_ccache:+/usr/lib/ccache/bin/}clang
    LD=ld.lld
    LLVM=1
    LLVM_IAS=1
  )
else
  BUILD_FLAGS+=(
    CC=${_ccache:+/usr/lib/ccache/bin/}gcc
  )
fi

prepare() {
  cd $_srcname

  echo "Setting version..."
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    src="${src%.zst}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  echo "Setting config..."
  cp ../config .config
  make "${BUILD_FLAGS[@]}" olddefconfig
  diff -u ../config .config || :

  if [[ ${_localmodconfig:+x} == x ]]; then
    echo "running make localmodconfig"
    if [[ ! -e "$_lsmod" ]]; then
      echo "WARNING: '$_lsmod' does not exist, generating it"
      lsmod | awk 'NR>1 { print $1 }' | sort > "$_lsmod"
    fi
    make "${BUILD_FLAGS[@]}" LSMOD="$_lsmod" localmodconfig
  fi

  [[ -z "$_nconfig" ]] || make "${BUILD_FLAGS[@]}" nconfig
  [[ -z "$_menuconfig" ]] || make "${BUILD_FLAGS[@]}" menuconfig
  [[ -z "$_xconfig" ]] || make "${BUILD_FLAGS[@]}" xconfig
  [[ -z "$_gconfig" ]] || make "${BUILD_FLAGS[@]}" gconfig  

  make -s kernelrelease > version
  echo "Saving config for later reuse"
  cp .config ../../config-"$(<version)"
  echo "Prepared $pkgbase version $(<version)"
}

build() {
  cd $_srcname
  make "${BUILD_FLAGS[@]}" all
  make "${BUILD_FLAGS[@]}" -C tools/bpf/bpftool vmlinux.h feature-clang-bpf-co-re=1
  #make "${BUILD_FLAGS[@]}" htmldocs
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(
    coreutils
    initramfs
    kmod
  )
  optdepends=(
    'wireless-regdb: to set the correct wireless channels of your country'
    'linux-firmware: firmware images needed for some devices'
  )
  provides=(
    KSMBD-MODULE
    UKSMD-BUILTIN
    VHBA-MODULE
    VIRTUALBOX-GUEST-MODULES
    WIREGUARD-MODULE
  )
  replaces=(
  )

  cd $_srcname
  local modulesdir="$pkgdir/usr/lib/modules/$(<version)"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  ZSTD_CLEVEL=19 make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 \
    DEPMOD=/doesnt/exist modules_install  # Suppress depmod

  # remove build link
  rm "$modulesdir"/build
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux tools/bpf/bpftool/vmlinux.h
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/x86" -a arch/x86/include
  install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */x86/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -Sib "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

_package-docs() {
  pkgdesc="Documentation for the $pkgdesc kernel"

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing documentation..."
  local src dst
  while read -rd '' src; do
    dst="${src#Documentation/}"
    dst="$builddir/Documentation/${dst#output/}"
    install -Dm644 "$src" "$dst"
  done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/share/doc"
  ln -sr "$builddir/Documentation" "$pkgdir/usr/share/doc/$pkgbase"
}

pkgname=(
  "$pkgbase"
  "$pkgbase-headers"
  #"$pkgbase-docs"
)
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
